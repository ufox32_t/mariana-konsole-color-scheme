# Mariana (Sublime HQ Pty Ltd, Dmitri Voronianski)

Color scheme for Konsole based in "Mariana" by "Sublime HQ Pty Ltd, Dmitri Voronianski".

![alt](https://i.imgur.com/fbVE96c.png)

![alt](https://i.imgur.com/kyAS0qn.png)


## How to install this theme

`$ git clone https://gitlab.com/ufox32_t/mariana-konsole-color-scheme.git`

`$ cd mariana-konsole-color-scheme`

`$ cp 'Mariana (Sublime HQ Pty Ltd, Dmitri Voronianski).colorscheme' ~/.local/share/konsole/`